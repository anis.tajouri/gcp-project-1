const assert = require('assert');
const sinon = require('sinon');
const uuid = require('uuid');
const {helloHttp} = require('..');
it('helloHttp: should print a name', () => {
 // Mock ExpressJS 'req' and 'res' parameters
 const name = uuid.v4();
 const req = {
   query: {},
   body: {
      name: name,
   },
 };
 const res = {send: sinon.stub()};
 // Call tested function
 helloHttp(req, res);
 // Verify behavior of tested function
 assert.ok(res.send.calledOnce);
 assert.deepStrictEqual(res.send.firstCall.args, [`Hello ${name}!`]);
});

